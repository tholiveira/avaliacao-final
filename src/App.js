import React from 'react';
import './reset.css'
import './geral.css'
import IndexColaboradores from './components/index/indexColaboradores'
import FormBase from './components/form/FormBase';

function App() {
  return (
    <React.Fragment>
        <FormBase/>
    </React.Fragment>
  );
}

export default App;
