import axios from 'axios';

const Api = axios.create({
    baseURL: "https://api-in.herokuapp.com/" ,
});

export default Api;