import React from 'react'
import PsIntro from '../psIntro'
import { Link } from 'react-router-dom';
import './Login.css'

export default function Login() {
    return (
        <React.Fragment>
            <div className="mainLogin">
                <PsIntro></PsIntro>
                <div className="mainForm">
                    <form  className="formLogin">
                        <p className="titleLogin">Área do treinamento</p>
                        <input className="inputFormLogin" placeholder="login"></input>
                        <input className="inputFormLogin" placeholder="senha" type="password"></input>
                        <button className="buttonFormLogin">Enviar</button>
                        <Link className="linkToRecover"to="/recover_password">Recuperar Senha</Link>
                    </form>
                </div>
            </div>
        </React.Fragment>
    )
}
