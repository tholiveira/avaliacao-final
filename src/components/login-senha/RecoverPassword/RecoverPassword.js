import React from 'react'
import './RecoverPassword.css'
import { Link } from 'react-router-dom';
import PsIntro from '../psIntro'


export default function RecoverPassword() {
    return (
        <React.Fragment>
            <div className="mainRecover">
                <PsIntro></PsIntro>
                <div className="mainForm">
                    <form className="formPassword">
                        <p className="titleForm">Recuperar Senha</p>
                        <input className="inputFormPassword"placeholder="e-mail" name="recoverPassword" id="RecoverPassword"></input>
                        <button className="buttonFormPassword">Enviar</button>
                        <Link className="linkToRecover" to="Login">Voltar</Link>
                    </form>    
                </div>
            </div>
        </React.Fragment>
    )
}
