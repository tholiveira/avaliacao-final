import React from 'react'
import "./psIntro.css"
import { Link } from "react-router-dom";

export default function psIntro() {
    return (
        <div className="psIntro">
            <div className="">
                <img src={require("./img/logo-IN.png")} alt="logoIN"></img>
            </div>
            <div className="textIntro">
                <h2>Processo Seletivo</h2>
                <p>Está aberto o processo seletivo de 2019.2</p>
                <p>As inscriões irão de 01/01 até 01/01.</p>
                <Link className="linktoWhite" to="/register">Inscreva-se</Link>
            </div>
        </div>
    )
}
