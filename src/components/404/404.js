import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../header/header';
import './404.css';

export default class _404 extends Component {
    
    handleClick(){
        window.history.go(-1)
    }

    render() {
        return (
            <React.Fragment>
                <Header></Header>
            <div className="main-404">
                <h1>404</h1>
                <img src={require("./img/404-b.png")} alt="Não existe"></img>
                <p>Oops, essa página não existe.</p>
                <span className="linkto"  onClick={() => this.handleClick()}>Voltar</span>
            </div>
            </React.Fragment>
        )
    }
}
