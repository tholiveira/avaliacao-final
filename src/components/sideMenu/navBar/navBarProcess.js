import React, { Component } from 'react'
import './navBar.css'
import { Link } from 'react-router-dom'

export default class navBar extends Component {
    
    render() {
        return (
            //necessário o id para ser passado o mesmo para função expand
            <nav id="process">
                <button className='titleCategory' onClick={() => this.props.expand('process')}><p>Processos Seletivos</p><img className="arrow normal" src={require("./arrow.svg")} alt="arrow"></img></button>
                <ul className='category'> 
                    <Link className="linktoHeader" to="/Wip"><li className='itemAccordion'>Processo Seletivo Vigente</li></Link>
                    <Link className="linktoHeader" to="/Wip"><li className='itemAccordion'>Todos os processos seletivos</li></Link>
                    <Link className="linktoHeader" to="/Wip"><li className='itemAccordion'>Criar processo seletivo</li></Link>
                </ul>
            </nav>
        )
    }
}
