import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './navBar.css'

export default class navBarUsers extends Component {

    render() {
        return (
            //necessário o id para ser passado o mesmo para função expand
            <nav id="users">
                <button className='titleCategory' id='btn_title' onClick={() => this.props.expand('users')}><p>Usuários</p><img className="arrow normal" src={require("./arrow.svg")} alt="arrow"></img></button>
                <ul className='category'> 
                    <Link className="linktoHeader" to="/users"><li className='itemAccordion'>Todos os usuários</li></Link>
                    <Link className="linktoHeader" to="/WiP"><li className='itemAccordion'>Candidatos</li></Link>
                    <Link className="linktoHeader" to="/colabs"><li className='itemAccordion'>Colaboradores</li></Link>
                    <Link className="linktoHeader" to="/register"><li className='itemAccordion'>Criar usuário</li></Link>
                </ul>
            </nav>
        )
    }
}
