import React from 'react'
import NavBarProcess from './navBar/navBarProcess'
import NavBarUsers from './navBar/navBarUsers'
import './sideMenu.css'

export default class sideMenu extends React.Component {

    expandOnClick = (id) => {
        const navbar = document.getElementById(id)//pega o menu navbar pelo ID enviado pelo navbar clicada
        
        const item = navbar.querySelector(".category");//seleciona a ul
        item.classList.toggle("accordion");
        const arrow = navbar.querySelector(".arrow");//seleciona a img 
        arrow.classList.toggle("rotate");
    };

    render() {
        return (
            <React.Fragment>
            <input id="check" type="checkbox"/>
            <div className="mainSideMenu">
            <button className='dashBoard'>DashBoard</button>    
                <NavBarProcess expand={this.expandOnClick}></NavBarProcess>
                <NavBarUsers expand={this.expandOnClick}></NavBarUsers>
            </div>
            </React.Fragment>
        )
    }
}

