import React, { Component } from 'react'
import Api from '../../Api'
import Header from '../header/header'
import SideMenu from '../sideMenu/sideMenu'
import './FormBase.css'

export default class FormBase extends Component {
    state = {
        user: {
            "name" : " ",
            "email" : " ",
            "password": " ",
            "kind" : " "
        }
    };

   handleOnSubmit(e){
       //   Api.post("/users").then(()=>{
           
           //  });
           alert("CLIQUEI");
           e.preventDefault();
   }
    
    render() {
        return (
            <React.Fragment>
                <Header/>
                    <div className="content">
                    <SideMenu/>
                        <div className="collum">
                            <div className="header-table">
                                <p>Cadastre-se</p>
                            </div>
                           <form className="register"onSubmit={this.handleSubmit}>
                               <div className="itensForm">
                                    <label>Nome:</label>
                                    <input type="text" placeholder="Digite seu nome"></input>
                               </div>
                               <div className="itensForm">
                                    <label>Email:</label>
                                    <input type="text" placeholder="Digite seu email"></input>
                               </div > 
                               <div className="itensForm">
                                    <label>Senha:</label>
                                    <input type="password"></input>
                               </div>
                               <div className="itensForm">
                                    <label>Cargo:</label>
                                    <select>
                                        <option value="0">Membro</option>
                                        <option value="1">Diretor</option>
                                        <option value="2">Ex-membro</option>
                                        <option value="3">Candidato</option>
                                        <option value="4">Colaborador Externo</option>
                                    </select>
                               </div>
                               <button className="submitButton" >Cadastre-se</button>
                           </form>
                        </div>
                    </div>
            </React.Fragment>
        )
    }
}
