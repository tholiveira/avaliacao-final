import React from 'react'
import './userTable.css'
import SearchBar from '../search/Search'
// import Layout from '../../layout/Layout'
import ReactDOM from 'react-dom'
import axios from 'axios';
import { BrowserRouter, Link } from 'react-router-dom';
import indexColaboradores from '../indexColaboradores';

    const api = axios.create({
        baseURL: "https://lobinhos-api.herokuapp.com"
    })
export default class colaboratorsTable extends React.Component {

    componentDidMount(){
        api.get('/wolves')
        .then(function (response) {
            console.log(response.data);
            const arq = document.querySelector("#bodyTable")
            
            for (let index = 0; index < response.data.length; index++) {
                const newRow = document.createElement("tr")
                const newName = document.createElement("td")
                newName.innerText = response.data[index].name
                newRow.appendChild(newName)
                const newType = document.createElement("td")
                newType.innerText = response.data[index].age
                newRow.appendChild(newType)
                const newStatus = document.createElement("td")
                const newStatusText = document.createElement("button")
                newStatusText.innerText = response.data[index].age
                if(response.data[index].age != null){
                    newStatusText.classList.add("status")
                    if(response.data[index].age>2){
                        newStatusText.classList.add("repproved")
                    }else if (response.data[index].age<=2){
                        newStatusText.classList.add("approved")
                    } 
                    newStatus.appendChild(newStatusText)
                }
                newRow.appendChild(newStatus)
                const newCel = document.createElement("td")
                ReactDOM.render(<BrowserRouter><Link to="/404" target={indexColaboradores} className='editButton'>editar</Link></BrowserRouter>, newCel);
                newRow.appendChild(newCel)
                arq.appendChild(newRow)
            }
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .finally(function () {
            // always executed
          });
    }
       
    render() {
        return (
            <table className="tableColaborator">
            <thead>
            <tr className="titlesTable"> 
                <th>Nome do usuário</th>
                <th>Tipo de usuário</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="bodyTable">
            </tbody>
        </table> 
        )
    }
}
