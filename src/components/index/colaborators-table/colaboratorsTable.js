import React from 'react'
import './colaboratorsTable.css'
import { BrowserRouter, Link } from 'react-router-dom';
import indexColaboradores from '../indexColaboradores';
import Api from '../../../Api'
import { userInfo } from 'os';

export default class colaboratorsTable extends React.Component {
    
    state = { colaborators:[] };

    componentDidMount(){
        Api.get('/users').then(response => {
            this.setState({colaborators: [... response.data]});
            console.log(response);
        });        
    }
    
    render() {
        const renderColaborators = this.state.colaborators.map((colaborator)=> {
            if(!colaborator.name) return null;
            return (
            <tr key={colaborator.id}> 
                <td>{colaborator.name}</td>
                <td>{colaborator.kind}</td>
                <td><Link className="editButton">editar</Link></td>
            </tr>
        )});

        return (
            <table className="tableColaborator">
            <thead>
            <tr className="titlesTable"> 
                <th>Nome do usuário</th>
                <th>Tipo de usuário</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="bodyTable">
                {renderColaborators}
            </tbody>
        </table> 
        )
    }
}
