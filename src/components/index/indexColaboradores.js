import React from 'react'
import Header from '../header/header'
import SideMenu from '../sideMenu/sideMenu'
import Colaboradores from './colaborators-table/colaboratorsTable'
import Search from './search/Search'
import './index.css'

export default class indexColaboradores extends React.Component {

    state = {
        search: " "
    };
    
    updateSearch(event) {
        this.setState({search: event.target.value.substr(0,20)});
    }

    render() {
        return (
            <React.Fragment>
                    <Header/>
                    <div className="content">
                    <SideMenu/>
                        <div className="collum">
                            <div className="header-table">
                                <p>Colaboradores</p>
                                <Search/>
                            </div>
                            <Colaboradores></Colaboradores>
                        </div>
                    </div>
            </React.Fragment>
        )
    }
}
