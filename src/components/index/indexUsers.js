import React from 'react'
import Header from '../header/header'
import SideMenu from '../sideMenu/sideMenu'
import Users from './users-table/userTable'
import Search from './search/Search'
import './index.css'

export default function indexUsers() {

    return (
        <React.Fragment>
            <Header/>
            <div className="content">
            <SideMenu/>
                <div className="collum">
                    <div className="header-table">
                        <p>Usuários</p>
                        <Search/>
                    </div>
                    <Users></Users>
                </div>
            </div>
        </React.Fragment>
    )
}
