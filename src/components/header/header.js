import React from 'react'
import SideMenu from '../sideMenu/sideMenu'
import './assets/header.css'
import { Link } from 'react-router-dom';

export default function header() {
    return (
        <header>
            <div className="area-header">
                <label for="check" className='sideArea'><img id="sideArrow" src={require('./assets/img/arrow.svg')} alt="LogoIN" ></img></label>
                <Link className='logo' to="/"><img src={require('./assets/img/logo-IN.png')} alt="LogoIN" ></img></Link>
                <div className="dropdown">
                    <input type="checkbox" id="checkconfig"/>
                    <label for="checkconfig" className="config-area">
                        <img src={require("./assets/img/config.svg")} alt="config" />
                    </label>
                    <div className="dropdown-content">
                        <ul>
                            <Link className="linktoHeader" to="/WiP"><li>Configurações</li></Link>
                            <Link className="linktoHeader" to="/WiP"><li>Log do sistema</li></Link>
                            <Link className="linktoHeader" to="/WiP"><li>Log da conta</li></Link>
                            <Link className="linktoHeader" to="/login"><li>Sair</li></Link>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    )
}
