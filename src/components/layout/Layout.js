import React, { Component } from 'react'
import SideMenu from '../sideMenu/sideMenu'
import Header from '../header/header'

export default class Layout extends Component {
    render(props) {
        return (
            <React.Fragment>
                <Header/> 
                <SideMenu/>
            </React.Fragment>
        )
    }
}
