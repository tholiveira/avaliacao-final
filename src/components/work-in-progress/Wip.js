import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../header/header'
import './Wip.css';

export default class Wip extends Component {

    handleClick(){
        window.history.go(-1)
    }

    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <div className="main-Wip">
                    <img src={require("./img/Wip-b.png")} alt="Em Construção"></img>
                    <p>Esta funcionabilidade ainda não foi implementada.</p>
                    <span className="linkto" onClick={() => this.handleClick()}>Voltar</span>
                </div>
            </React.Fragment>
        )
    }
}