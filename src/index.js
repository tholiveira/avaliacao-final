import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import _404 from './components/404/404'
import Login from './components/login-senha/Login/Login';
import RecoverPassword from './components/login-senha/RecoverPassword/RecoverPassword';
import Wip from './components/work-in-progress/Wip'
import navBar from './components/sideMenu/sideMenu';
import indexColaboradores from './components/index/indexColaboradores';
import indexUsers from './components/index/indexUsers';
import FormBase from './components/form/FormBase';

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={App} />
            <Route path="/login" exact={true} component={Login} />
            <Route path="/wip" exact={true} component={Wip} />
            <Route path="/recover_password" exact={true} component={RecoverPassword} />
            <Route path="/navbar" exact={true} component={navBar} />
            <Route path="/colabs" exact={true} component={indexColaboradores} />
            <Route path="/users" exact={true} component={indexUsers} />
            <Route path="/register" exact={true} component={FormBase} />
            <Route path='*' component={_404} />
        </Switch>
    </ BrowserRouter>, 
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
